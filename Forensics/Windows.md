# Memory-Dumping
- [dump-virtual-box-memory](https://www.ired.team/miscellaneous-reversing-forensics/dump-virtual-box-memory)
## Convert between image types
Reference: https://docs.openstack.org/image-guide/convert-images.html
```
qemu-img convert
```
# Devices management
[Introduction to udev](https://opensource.com/article/18/11/udev)